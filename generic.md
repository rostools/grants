---
title: "Mission statement and generic template for grant applications"
---

> STILL A DRAFT

# Vision

Our dream is a future where the default for science is to be reproducible and
open. We hope that one day, researchers will conduct their scientific activities
following open scientific principles not as an active choice but because it is
the proper and *easier* way of doing science.

Our projects focus within three branches: software, teaching, and support networks.

- For our software, we aim to automate what we can, simplify and streamline the
rest. We aspire to be similar to [`devtools`][^devtools] for doing reproducible
and open science, which is an R software package designed to make it easier to
create other R packages.
- For our learning material, we aspire to be the "go to" reference for learning
the *exact steps and processes* for doing reproducible and open science, and for
knowing and staying updated on which tools and services to use.
- For our support network, we hope to link and connect with all the amazing
research groups throughout the world who are working hard to practice and
promote reproducible and open science. Through this connection, we hope that it
will provide others with real-world examples and role models for how science 
should be done.

[`devtools`]: https://devtools.r-lib.org/

We are (mostly) biomedical researchers, and as such our current focus is on
biomedical science.

# Mission

To acheive our vision, our mission is to create a highly opinionated, practical,
and process-oriented ecosystem of software tools and accompanying documentation,
tutorials, and learning materials, supported by a network of practitioners
(researchers), that informs on how to conduct open and reproducible science.

Our mission is divided into multiple parts based on the organizations' branches:

- Software: We aim to create an ecosystem of interconnected packages and software
tools, by either developing these tools or by linking existing ones. This ecosystem
of tools aims to reduce the burden on researchers in conducting open and reproducible
science through automation and programming. We hope to emulate the principles
and structure of the [tidyverse] ecosystem.
- Learning material: (COMPLETE)
- Support network: (COMPLETE)

[tidyverse]: https://www.tidyverse.org/

In all project branches, our aim is to design the software and learning material
to be comprehensive, carefully considered, and scientifically-informed by emphasizing
usability and simplicity.

Our current focus is on the data analysis and publication side of scientific
activities.

# Objectives

(Need this?)

# Project branches

- Scientific software development
- Curriculum development and instruction
- Supportive network and list of resources

All branches will include research projects that assess effectiveness,
usability, learnability, and best practices.

# Tasks and duties

- Research best practices for learning and teaching
    - How to best learn, remember, and apply skills gained
    - Literature reviews, experimental setups for testing teaching and
    curriculum, surveys and longitudinal follows to identify later use
- Research best practices for software use and design
    - Best practices for designing usable, friendly, and intuitive designs and
    interfaces
    - Best practices for maintanence and development
        - Workflows, project management, etc
    - Literature reviews, testing of usage

# Potential funding and income sources

(NOTE: Maybe create a non-profit organization to structure these sources and
activities?)

- Developing books and publishing via e.g. [leanpub]
- Running workshops for corporate groups, general public, and academic audiences
(different fee tiers for each)
- Online courses, either free or charged via e.g. [leanpub] (others)?
- Through Patreon subscribers
- From research funding agencies
    - In DK: Villum, Carlsberg
- From donations?
- Innovation funds from e.g. Novo Nordisk Fonden, Danish Research Fonden, AU?

[leanpub]: https://leanpub.com/

# The (ideal or possible) team

Network of research collaborators:

- (From UofTCoders)

Full-time positions for (in order of hiring):

- R programmer
    - Package development for statistical, analytical, and workflow processes
    and methods
    - Develop packages designed to simplify reproducibility and open science in
    a biomedical context, considering privacy concerns, with an evidence-based 
    effective user interface
    - Contribute to commonly used open source R packages, such as in the tidyverse

- Curriculum developer and instructor
    - Develop curriculum targeted specifically at biomedical researchers
    - Contribute to (and make use of) other openly licensed teaching material
    such as Carpentries, RStudio, Open Science MOOC, and other R material (e.g.
    R4DS, etc)
    - Run and instruct regular data analysis workshops

- Coordinator (name?)
    - Organize meetings, retreats, code sprints, workshops, etc
    - Marketing and advertising
    - Promote teaching material 
    - Identify and recruit passionate researchers interested in participating
    and/or running workshops

- Researchers 
    - Either as postdocs, PhD students, or from our research support network
    - Conduct research (analyses, writing) of research projects based on data
    collected from studies exploring teaching effectiveness, package/software
    utilization, and efficient and usable workflows.

All will be encouraged (during work time) to answer questions on [StackOverflow]
and [CrossValidated] Q&A sites, to contribute in whatever way to [tidyverse]
packages or documentation, and to be involved in or participant in whatever way
with sibling projects/organizations (e.g. [The Turing Way] and the [Open Science MOOC]).
Likewise, it is expected to continue learning as this area is actively and rapidly 
improving and progressing. So, a non-insignicant amount of time during working 
hours is to learning new skills and knowledge and to record this learning in
some written format like blogs.

[Open Science MOOC]: https://opensciencemooc.eu/
[CrossValidated]: https://stats.stackexchange.com/
[StackOverflow]: https://stackoverflow.com/
[The Turing Way]: https://the-turing-way.netlify.com/introduction/introduction
