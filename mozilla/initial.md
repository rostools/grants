
# Project title

Simplifying and automating reproducible open science: A combined toolkit and training material

# Anticipated Project Start Date

March 1st, 2019

# Duration of the project (in months) 

6 months

# Amount Requested (USD)

$8200

# Total Project Budget (USD)

Estimated costs:

- R programmer: 80 USD/hr x 90hr = 7200 USD
    - Range for Data Science type work: 25-200 USD depending on task
    - Based on [this blog](https://www.upwork.com/hiring/data/how-much-hire-data-scientist/)
- Editing services: 40 USD/hr x 25hr = 1000 USD
    - Range depending on editing phase: 25-80 USD
    - Estimate from [this site](https://mirandamarquit.com/how-should-you-charge-for-freelance-editing/)
    
**Total**: 7200 + 1000 = 8200

# Project Purpose

- Provide a 140 character description of your concept that could be used to
explain your work to a new audience.

*Characters: 140 / 140*

To create an (opinionated) toolkit to simplify and automate doing reproducible and open science, along with accompanying training material.

# Project Summary

- Describe your project and its goals. If you are responding to a specific Awards
Track, how does your approach advance the goals of that Awards Track?

*Characters: 2024 / 2500*

Reproducible open scientific practices in the biomedical research community are far from being widely adopted. Part of the issue stems from a lack of awareness and training on how to do open science. There are also perceived problems with intellectual property, particularly in the biomedical community, fueled by the fear of being "scooped". Funding and reward structures that do not consider open science further intensify this issue. While many of these issues require large-scale, concerted efforts to fix, some can be tackled through grassroots initiatives, such as those that stem from a lack of a) awareness, b) tools that are easy to use and to learn, and c) training on how to do open science.

Open science practices and setup procedures can often be perceived as more technically challenging, especially for those researchers new to the concept of working in the open. Open science consists of a wide array of rapidly evolving tools, which can make it challenging to choose the right one for the job and to keep up to date with the latest best practices. Our proposed project aims to target some of these issues by creating:

1. An opinionated set of tools and workflows that simplify and automate many common reproducible open scientific tasks, focusing more on data analysis, manuscript writing, and publication tasks.
2. A corresponding set of tutorials and course material on using these tools and workflows.

This set of tools and workflows will bundle together already existing and outstanding open source software, guidelines, and best practices, so that it is easier for the novice or curious "open science practitioner" to use these resources. Our project's goal is to make reproducible and open science more accessible for researchers to use and to learn. We hope that this project will bring open science practices closer to being the default choice rather than an alternative for enthusiasts.

Our project targets specifically the *Prototyping* and *Curriculum* areas of the Open Science Mini-Grant call.

# Deliverables

- What will be produced as a result of your activities over the award period? Your
response should be a numbered list.

*Characters: 617 / 1250*

1. A short document (a "manifesto") detailing the (opinionated) tools, processes, and resources to use when doing reproducible and open science. Open access publication outlet: [Research Ideas and Outcomes (RIO) Journal](https://riojournal.com/)  or [Journal of Open Source Software (JOSS)](https://joss.theoj.org/).
2. An initial version release of an R software package that automates and simplifies several steps in the open science workflow. Package repository and outlets: GitHub, [CRAN](https://cran.r-project.org/), and [rOpenSci](https://ropensci.org/)
3. A first, initial version of accompanying training material for using 1) and 2). Outlet: GitHub Pages hosted ebook or an public and free DataCamp tutorial.

# Suitability

- Why are you or your organization best suited to advance this work? What else
have you done in this space? Include links to other projects and previous work
as applicable. Please also note if you are working with any team members or
partners, explaining how these collaborations advance your project.

*Characters: 2101 / 2500*

We are two researchers (Luke Johnston and Joel Ostblom) who are passionate about doing open science. Both of us are in the biomedical research field, so we live and experience the issues described above everyday with colleagues and others in our respective fields (diabetes epidemiology and biomedical engineering). We have both completed the Mozilla Open Project Leader training program (in Round 5), and so are familiar with working in the open. Joel was a mentor and host in Round 6. We are invested in being more open, we have been adopting more open scientific practices such as using version control, publicly uploading and archiving our research code, and submitting our manuscripts into preprint archives, so we are familiar with these practices. Luke has already started working on a subset of this project (the toolkit side) from the Open Project Leader program with [prodigenr](https://github.com/lwjohnst86/prodigenr) as well as expanding on it with the (in-development) [rostools](https://github.com/lwjohnst86/rostools) package.

We are both heavily invested in making it easier for others to do open science. We have previously collaborated as executive members of the student organization [UofTCoders](https://uoftcoders.github.io/) at the University of Toronto (which is a branch of the Mozilla Open Science Lab Study Groups), where we taught graduate students how to use open tools to improve their research. In this capacity, we and other executive members co-developed and taught a 4th year undergraduate [course](https://uoftcoders.github.io/rcourse/) focused on how to conduct reproducible open science and how to analyze data using R. Luke has also started another study group, the [Open Coders](https://au-oc.github.io/), at his current position in Aarhus University in Denmark that (like the UofTCoders) aims at teaching reproducible data analysis skills.

Through these collaborations, we discovered our shared passion for spreading open science, which we now intend to develop further. We have benefited from our complementary expertise in R and Python and believe that our combined skills will help us reach a bigger scientific community, particularly since R and Python are the most commonly used open source languages in scientific data analysis.
